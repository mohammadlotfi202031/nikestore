package com.example.nikestore.view.scroll;

public enum ScrollState {
    STOP,
    UP,
    DOWN,
}