package com.example.nikestore.data.repo.banner

import com.example.nikestore.data.Banner
import com.example.nikestore.data.repo.sourse.BannerDataSource
import io.reactivex.Single

class BannerRepositoryImpl(val bannerRemoteDataSource: BannerDataSource) : BannerRepository {
    override fun getBanners(): Single<List<Banner>> {
        return bannerRemoteDataSource.getBanners()
    }
    //override fun getBanners(): Single<List<Banner>> =bannerRemoteDataSource.getBanners()

}