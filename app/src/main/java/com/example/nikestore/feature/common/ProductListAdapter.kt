package com.example.nikestore.feature.common

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.nikestore.R
import com.example.nikestore.common.formatPrice
import com.example.nikestore.common.implementSpringAnimationTrait
import com.example.nikestore.data.Product
import com.example.nikestore.services.ImageLoadingService
import com.example.nikestore.view.NikeImageView
import java.lang.IllegalStateException

const val View_TYPE_ROUND=0
const val View_TYPE_SMALL=1
const val View_TYPE_LARGE=2

class ProductListAdapter(var viewType: Int= View_TYPE_ROUND, val imageLoadingService: ImageLoadingService) :
    RecyclerView.Adapter<ProductListAdapter.ViewHolder>() {

    var productEventListener:ProductEventListener?=null
    var products= ArrayList<Product>()
    set(value)
    {
        field = value
        notifyDataSetChanged()

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val productIv: NikeImageView = itemView.findViewById(R.id.product_iv)
        val titleTv: TextView = itemView.findViewById(R.id.product_title_tv)
        val currentPriceTv: TextView = itemView.findViewById(R.id.currentPriceTv)
        val previousPriceTv: TextView = itemView.findViewById(R.id.previousPriceTv)
        val favorite: ImageView = itemView.findViewById(R.id.favoriteBtn)

        fun bindProduct(product: Product) {
            imageLoadingService.load(productIv, product.image)
            titleTv.text = product.title
            currentPriceTv.text = formatPrice(product.price)
            previousPriceTv.text = formatPrice(product.previous_price)
            previousPriceTv.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG


            if (product.isFavorite){
                favorite.setImageResource(R.drawable.ic_favorite_fill)
            }else{
                favorite.setImageResource(R.drawable.ic_favorites)
            }

            itemView.implementSpringAnimationTrait()
            itemView.setOnClickListener{
                productEventListener?.onProductClick(product)

            }

            favorite.setOnClickListener {
                productEventListener?.onFavoriteBtnClick(product)
                product.isFavorite=!product.isFavorite
                notifyItemChanged(adapterPosition)
            }

        }
    }

    override fun getItemViewType(position: Int): Int {
        return viewType
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutResId=when(viewType){
            View_TYPE_ROUND -> R.layout.item_product
            View_TYPE_LARGE -> R.layout.item_product_large
            View_TYPE_SMALL -> R.layout.item_product_small
            else -> throw IllegalStateException("viewType is not valid")
        }
        return ViewHolder(LayoutInflater.from(parent.context).inflate(layoutResId,parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindProduct(products[position])
    }

    override fun getItemCount(): Int {
        return products.size
    }

    interface ProductEventListener{
        fun onProductClick(product: Product)
        fun onFavoriteBtnClick(product: Product)
    }

}