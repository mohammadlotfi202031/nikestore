package com.example.nikestore.feature.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nikestore.R
import com.example.nikestore.common.EXTRA_KEY_DATA
import com.example.nikestore.common.NikeFragment
import com.example.nikestore.common.convertDpToPixel
import com.example.nikestore.data.*
import com.example.nikestore.feature.common.ProductListAdapter
import com.example.nikestore.feature.common.View_TYPE_ROUND
import com.example.nikestore.feature.listproduct.ProductListActivity
import com.example.nikestore.feature.main.BannerSliderAdapter
import com.example.nikestore.feature.product.ProductDetailActivity
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import timber.log.Timber
import kotlin.collections.ArrayList

class HomeFragment : NikeFragment(), ProductListAdapter.ProductEventListener {

    val homeViewModel: HomeViewModel by viewModel()
    val productListAdapterLatest: ProductListAdapter by inject { parametersOf(View_TYPE_ROUND)}
    val productListAdapterPopular: ProductListAdapter by inject{ parametersOf(View_TYPE_ROUND)}
    val productListAdapterPriceAsc: ProductListAdapter by inject{ parametersOf(View_TYPE_ROUND)}
    val productListAdapterPriceDesc: ProductListAdapter by inject{ parametersOf(View_TYPE_ROUND)}


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewLatestProductaBtn.setOnClickListener{
            startActivity(Intent(requireContext(),ProductListActivity::class.java).apply {
              putExtra(EXTRA_KEY_DATA, SORT_LATEST)
            })
        }

        viewPopularProductaBtn.setOnClickListener {
            startActivity(Intent(requireContext(),ProductListActivity::class.java).apply {
                putExtra(EXTRA_KEY_DATA, SORT_POPULAR)
            })
        }
        viewAscProductaBtn.setOnClickListener {
            startActivity(Intent(requireContext(),ProductListActivity::class.java).apply {
                putExtra(EXTRA_KEY_DATA, SORT_PRICE_ASC)
            })
        }
        viewDescProductaBtn.setOnClickListener {
            startActivity(Intent(requireContext(),ProductListActivity::class.java).apply {
                putExtra(EXTRA_KEY_DATA, SORT_PRICE_DESC)
            })
        }


        latestProductsRv.layoutManager=LinearLayoutManager(requireContext(),RecyclerView.HORIZONTAL,false)
        latestProductsRv.adapter=productListAdapterLatest
        popularProductsRv.layoutManager=LinearLayoutManager(requireContext(),RecyclerView.HORIZONTAL,false)
        popularProductsRv.adapter=productListAdapterPopular
        descProductsRv.layoutManager=LinearLayoutManager(requireContext(),RecyclerView.HORIZONTAL,false)
        descProductsRv.adapter=productListAdapterPriceDesc
        ascProductsRv.layoutManager=LinearLayoutManager(requireContext(),RecyclerView.HORIZONTAL,false)
        ascProductsRv.adapter=productListAdapterPriceAsc

        productListAdapterLatest.productEventListener=this
        productListAdapterPopular.productEventListener=this
        productListAdapterPriceDesc.productEventListener=this
        productListAdapterPriceAsc.productEventListener=this


        homeViewModel.productLiveData.observe(requireActivity()) {
            productListAdapterLatest.products=it as ArrayList<Product>
        }

        homeViewModel.productLiveDataPopular.observe(viewLifecycleOwner){
            productListAdapterPopular.products=it as ArrayList<Product>
        }

        homeViewModel.productLiveDataPriceDesc.observe(viewLifecycleOwner){
            productListAdapterPriceDesc.products=it as ArrayList<Product>
        }
        homeViewModel.productLiveDataPriceAsc.observe(viewLifecycleOwner){
            productListAdapterPriceAsc.products=it as ArrayList<Product>
        }
        homeViewModel.progressBarLiveData.observe(viewLifecycleOwner) {
            setProgressIndicator(it)
        }
        homeViewModel.bannerLiveData.observe(requireActivity()) {
            Timber.i(it.toString())
            val bannerSliderAdapter = BannerSliderAdapter(this, it)
            bannerSliderViewPager.adapter = bannerSliderAdapter

            val viewPagerHeight = (((bannerSliderViewPager.width - convertDpToPixel(
                32F,
                requireContext()
            )) * 173) / 328).toInt()
            val layoutParams = bannerSliderViewPager.layoutParams
            layoutParams.height = viewPagerHeight
            bannerSliderViewPager.layoutParams = layoutParams
            sliderIndicator.setViewPager2(bannerSliderViewPager)
            //تکرار
//            val timer = Timer()
//            timer.schedule(object : TimerTask() {
//                override fun run() {
//                    if (bannerSliderViewPager.currentItem < bannerSliderAdapter.itemCount - 1)
//                        bannerSliderViewPager.setCurrentItem(
//                            bannerSliderViewPager.currentItem + 1,
//                            true
//                        )
//                    else
//                        bannerSliderViewPager.setCurrentItem(0, true)
//                }
//            }, 3000, 3000)

        }
    }

    override fun onProductClick(product: Product) {
        startActivity(Intent(requireContext(),ProductDetailActivity::class.java).apply {
            putExtra(EXTRA_KEY_DATA,product)
        })
    }

    override fun onFavoriteBtnClick(product: Product) {
        homeViewModel.addProductToFavorites(product)
    }

}