package com.example.nikestore.data.repo.sourse

import com.example.nikestore.data.Comment
import io.reactivex.Single

interface CommentDataSource {

    fun getComments(productId:Int): Single<List<Comment>>

    fun addComment(): Single<Comment>

}