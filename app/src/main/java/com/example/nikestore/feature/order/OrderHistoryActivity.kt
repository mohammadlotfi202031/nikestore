package com.example.nikestore.feature.order

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nikestore.R
import com.example.nikestore.common.NikeActivity
import kotlinx.android.synthetic.main.activity_order_history.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import android.view.View
import kotlinx.android.synthetic.main.item_purchase_details.*

class OrderHistoryActivity : NikeActivity() {
    val viewModel: OrderHistoryViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_history)

        viewModel.progressBarLiveData.observe(this) {
            setProgressIndicator(it)
        }

        orderHistoryRv.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        viewModel.orders.observe(this) {
            orderHistoryRv.adapter = OrderHistoryItemAdapter(it,this)
        }

        toolbar_orderHistory.onBackButtonClickListener=View.OnClickListener {
            finish()
        }
    }
}