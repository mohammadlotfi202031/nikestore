package com.example.nikestore.feature.favorite

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nikestore.R
import com.example.nikestore.common.EXTRA_KEY_DATA
import com.example.nikestore.common.NikeActivity
import com.example.nikestore.data.Product
import com.example.nikestore.feature.product.ProductDetailActivity
import kotlinx.android.synthetic.main.activity_favorite_products.*
import kotlinx.android.synthetic.main.favorite_empty.*
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject

class FavoriteProductsActivity : NikeActivity(),
    FavoriteProductsAdapter.FavoriteProductEventListener {

    val viewModel: FavoriteProductsViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite_products)

        helpBtn.setOnClickListener {
            showSnackBar(getString(R.string.favorites_help_message))
        }

        toolbar_favorite.onBackButtonClickListener=View.OnClickListener {
            finish()
        }

        viewModel.productLiveData.observe(this) {
            if (it.isNotEmpty()){
                favoriteProducts_Rv.layoutManager =
                    LinearLayoutManager(this, RecyclerView.VERTICAL, false)
                favoriteProducts_Rv.adapter =
                    FavoriteProductsAdapter(it as MutableList<Product>, this, get())

            }else{
                showEmptyState(R.layout.favorite_empty)
                emptyStateMessageTv.text=getString(R.string.favorite_empty_state_message)
            }

        }


    }

    override fun onClick(product: Product) {
        startActivity(Intent(this,ProductDetailActivity::class.java).apply {
            putExtra(EXTRA_KEY_DATA,product)
        })
    }

    override fun onLongClick(product: Product) {
        viewModel.removeFromFavorites(product)
        Toast.makeText(this,"حذف شد",Toast.LENGTH_SHORT).show()
    }
}