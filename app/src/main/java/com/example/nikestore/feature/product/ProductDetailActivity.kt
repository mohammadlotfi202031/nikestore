package com.example.nikestore.feature.product

import  android.content.Intent
import android.graphics.Paint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.compose.runtime.currentCompositeKeyHash
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nikestore.R
import com.example.nikestore.common.*
import com.example.nikestore.data.Comment
import com.example.nikestore.feature.product.comment.CommentListActivity
import com.example.nikestore.services.ImageLoadingService
import com.example.nikestore.view.scroll.ObservableScrollViewCallbacks
import com.example.nikestore.view.scroll.ScrollState
import com.google.android.material.snackbar.Snackbar
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_product_detail.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ProductDetailActivity : NikeActivity() {
    val productDitailViewModel: ProductDitailViewModel by viewModel { parametersOf(intent.extras) }
    val imageLoadingService: ImageLoadingService by inject()
    val commentAdapter = CommentAdapter()
    val compositeDisposable = CompositeDisposable()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)

        productDitailViewModel.productLivedata.observe(this) {
            imageLoadingService.load(productIv_detail, it.image)
            productTtileTv_detail.text = it.title
            toolbarTitleTv.text = it.title
            previousPriceTv_detail.text = formatPrice(it.previous_price)
            previousPriceTv_detail.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            currentPriceTv_detail.text = formatPrice(it.price)

        }

        back_btn.setOnClickListener {
            finish()
        }


        productDitailViewModel.progressBarLiveData.observe(this) {
            setProgressIndicator(it)
        }
        productDitailViewModel.commentsLiveData.observe(this) {
            commentAdapter.comments = it as ArrayList<Comment>
            if (it.size > 3) {
                viewAllCommentBtn.visibility = View.VISIBLE
                viewAllCommentBtn.setOnClickListener {
                    startActivity(Intent(this, CommentListActivity::class.java).apply {
                        putExtra(EXTRA_KEY_ID, productDitailViewModel.productLivedata.value!!.id)
                    })
                }
            }
        }
        initViews()

    }


    fun initViews() {

        commentRv.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        commentRv.adapter = commentAdapter


        productIv_detail.post {
            val productIvHeight = productIv_detail.height
            val toolbarView = toolbarView_detail
            val productImageView = productIv_detail
            observableScrollView.addScrollViewCallbacks(object : ObservableScrollViewCallbacks {
                override fun onScrollChanged(
                    scrollY: Int,
                    firstScroll: Boolean,
                    dragging: Boolean
                ) {
                    toolbarView.alpha = scrollY.toFloat() / productIvHeight.toFloat()
                    productImageView.translationY = scrollY.toFloat() / 2
                }

                override fun onDownMotionEvent() {
                }

                override fun onUpOrCancelMotionEvent(scrollState: ScrollState?) {
                }
            })
        }

        addToCartBtn.setOnClickListener {
            productDitailViewModel.onAddToCartBtn()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : NikeCompletableObserver(compositeDisposable) {
                    override fun onComplete() {
                        showSnackBar(getString(R.string.success_addToCart))
                    }
                })


        }

    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

}