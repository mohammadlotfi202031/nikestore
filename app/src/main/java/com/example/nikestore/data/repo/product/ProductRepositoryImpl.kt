package com.example.nikestore.data.repo.product

import com.example.nikestore.data.Product
import com.example.nikestore.data.repo.sourse.ProductDataSourse
import com.example.nikestore.data.repo.sourse.ProductLocalDataSource
import io.reactivex.Completable
import io.reactivex.Single

class ProductRepositoryImpl(
    val remoteDataSourse: ProductDataSourse,
    val localDataSource: ProductLocalDataSource
) : ProductRepository {

    override fun getProducts(sort: Int): Single<List<Product>> {
        return localDataSource.getFavoriteProducts()
            .flatMap { favoriteProducts ->
                remoteDataSourse.getProducts(sort)
                    .doOnSuccess {
                        val favoriteProductIds = favoriteProducts.map {
                            it.id
                        }
                        it.forEach {product ->
                            if (favoriteProductIds.contains(product.id)){
                                product.isFavorite=true
                            }
                        }
                    }
            }
    }

    override fun getFavoriteProducts(): Single<List<Product>> {
        return localDataSource.getFavoriteProducts()
    }

    override fun addToFavorites(product: Product): Completable {
        return localDataSource.addToFavorites(product)
    }

    override fun deleteFromFavorites(product: Product): Completable {
        return localDataSource.deleteFromFavorites(product)
    }
}