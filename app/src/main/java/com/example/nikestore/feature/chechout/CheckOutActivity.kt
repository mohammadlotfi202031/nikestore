package com.example.nikestore.feature.chechout

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.nikestore.R
import com.example.nikestore.common.EXTRA_KEY_DATA
import com.example.nikestore.common.EXTRA_KEY_ID
import com.example.nikestore.common.formatPrice
import com.example.nikestore.feature.main.MainActivity
import com.example.nikestore.feature.order.OrderHistoryActivity
import kotlinx.android.synthetic.main.activity_check_out.*
import kotlinx.android.synthetic.main.item_purchase_details.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class CheckOutActivity : AppCompatActivity() {

    val checkoutViewModel:CheckoutViewModel by viewModel{
        val uri:Uri?=intent.data
        if (uri!=null){
            parametersOf(uri.getQueryParameter("order_id")!!.toInt())
        }else{
            parametersOf(intent.extras!!.getInt(EXTRA_KEY_ID))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_out)

        returnHomeBtn.setOnClickListener {
            startActivity(Intent(this,MainActivity::class.java))
        }
        orderHistoryBtn.setOnClickListener {
            startActivity(Intent(this,OrderHistoryActivity::class.java))
        }

        toolbar_checkOut.onBackButtonClickListener=View.OnClickListener {
            finish()
        }

        checkoutViewModel.checkoutLiveData.observe(this){
            orderPriceTv.text= formatPrice(it.payable_price)
            orderStatusTv.text=it.payment_status
            purchaseStatusTv.text=if (it.purchase_success) "خرید با موفقیت انجام شد" else "خرید ناموفق"
        }

    }
}