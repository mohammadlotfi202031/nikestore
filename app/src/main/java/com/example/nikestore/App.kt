package com.example.nikestore

import android.app.Application
import android.content.SharedPreferences
import android.os.Bundle
import androidx.room.Room
import com.example.nikestore.data.OrderHistoryItem
import com.example.nikestore.data.db.AppDataBase
import com.example.nikestore.data.repo.*
import com.example.nikestore.data.repo.banner.BannerRepository
import com.example.nikestore.data.repo.banner.BannerRepositoryImpl
import com.example.nikestore.data.repo.cart.CartRepository
import com.example.nikestore.data.repo.cart.CartRepositoryImpl
import com.example.nikestore.data.repo.comment.CommentRepository
import com.example.nikestore.data.repo.comment.CommentRepositoryImpl
import com.example.nikestore.data.repo.order.OrderRemoteDataSource
import com.example.nikestore.data.repo.order.OrderRepository
import com.example.nikestore.data.repo.order.OrderRepositoryImpl
import com.example.nikestore.data.repo.product.ProductRepository
import com.example.nikestore.data.repo.product.ProductRepositoryImpl
import com.example.nikestore.data.repo.sourse.*
import com.example.nikestore.data.repo.user.UserRepository
import com.example.nikestore.data.repo.user.UserRepositoryImpl
import com.example.nikestore.feature.auth.AuthViewModel
import com.example.nikestore.feature.cart.CartViewModel
import com.example.nikestore.feature.chechout.CheckoutViewModel
import com.example.nikestore.feature.common.ProductListAdapter
import com.example.nikestore.feature.favorite.FavoriteProductsViewModel
import com.example.nikestore.feature.listproduct.ProductListViewModel
import com.example.nikestore.feature.home.HomeViewModel
import com.example.nikestore.feature.main.MainViewModel
import com.example.nikestore.feature.order.OrderHistoryViewModel
import com.example.nikestore.feature.product.ProductDitailViewModel
import com.example.nikestore.feature.product.comment.CommentListViewModel
import com.example.nikestore.feature.profile.ProfileViewModel
import com.example.nikestore.feature.shipping.ShippingViewModel
import com.example.nikestore.services.FrescoImageLoadingServiceImpl
import com.example.nikestore.services.ImageLoadingService
import com.example.nikestore.services.http.ApiService
import com.example.nikestore.services.http.createApiServiceInstanse
import com.facebook.drawee.backends.pipeline.Fresco
import org.koin.android.ext.android.get
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())

        Fresco.initialize(this)

        val myMadules = module {
            single<ApiService> { createApiServiceInstanse() }
            single<ImageLoadingService> { FrescoImageLoadingServiceImpl() }
            single { Room.databaseBuilder(this@App,AppDataBase::class.java,"db_nike_store").build() }

            factory<ProductRepository> {
                ProductRepositoryImpl(
                    ProductRemoteDataSource(get()),
                    get<AppDataBase>().productDao()
                )
            }
            factory<BannerRepository> {
                BannerRepositoryImpl(
                    BannerRemoteDataSource(get())
                )
            }

            single<SharedPreferences> {
                this@App.getSharedPreferences(
                    "app_settings",
                    MODE_PRIVATE
                )
            }

            single { UserLocalDataSource(get()) }

            single<UserRepository> {
                UserRepositoryImpl(
                    UserRemoteDataSource(get()),
                    UserLocalDataSource(get())
                )
            }

            single<OrderRepository> { OrderRepositoryImpl(OrderRemoteDataSource(get())) }

            factory { (viewType: Int) -> ProductListAdapter(viewType, get()) }
            factory<CommentRepository> { CommentRepositoryImpl(CommentRemoteDataSource(get())) }
            factory<CartRepository> { CartRepositoryImpl(CartRemoteDataSource(get())) }

            viewModel { HomeViewModel(get(), get()) }
            viewModel { (bundle: Bundle) -> ProductDitailViewModel(bundle, get(), get()) }
            viewModel { (productId: Int) -> CommentListViewModel(productId, get()) }
            viewModel { (sort: Int) -> ProductListViewModel(sort, get()) }
            viewModel { AuthViewModel(get()) }
            viewModel { CartViewModel(get()) }
            viewModel { MainViewModel(get()) }
            viewModel { ShippingViewModel(get()) }
            viewModel { (orderId:Int)->CheckoutViewModel(orderId, get()) }
            viewModel { ProfileViewModel(get())}
            viewModel{FavoriteProductsViewModel(get())}
            viewModel{OrderHistoryViewModel(get())}

        }

        startKoin {
            androidContext(this@App)
            modules(myMadules)
        }

        val userRepository: UserRepository = get()
        userRepository.loadToken()

    }

}