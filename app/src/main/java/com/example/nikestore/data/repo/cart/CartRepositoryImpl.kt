package com.example.nikestore.data.repo.cart

import com.example.nikestore.data.AddToCartResponse
import com.example.nikestore.data.CartItemCount
import com.example.nikestore.data.CartResponse
import com.example.nikestore.data.MessageResponse
import com.example.nikestore.data.repo.sourse.CartDataSource
import io.reactivex.Single

class CartRepositoryImpl(val remoteDataSource: CartDataSource): CartRepository {
    override fun addToCart(productId: Int): Single<AddToCartResponse> {
        return remoteDataSource.addToCart(productId)
    }

    override fun get(): Single<CartResponse> {
        return remoteDataSource.get()
    }

    override fun remove(cartItemId: Int): Single<MessageResponse> {
        return remoteDataSource.remove(cartItemId)
    }

    override fun changeCount(cartItemId: Int, count: Int): Single<AddToCartResponse> {
        return remoteDataSource.changeCount(cartItemId,count)
    }

    override fun getCartItemCount(): Single<CartItemCount> {
        return remoteDataSource.getCartItemCount()
    }
}