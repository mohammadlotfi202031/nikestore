package com.example.nikestore.data.repo.sourse

import com.example.nikestore.data.Banner
import com.example.nikestore.services.http.ApiService
import io.reactivex.Single

class BannerRemoteDataSource(val apiSetvice:ApiService):BannerDataSource {

    override fun getBanners(): Single<List<Banner>> {
        return apiSetvice.getBanners()
    }

}