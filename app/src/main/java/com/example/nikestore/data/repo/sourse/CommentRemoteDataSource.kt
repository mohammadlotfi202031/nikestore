package com.example.nikestore.data.repo.sourse

import com.example.nikestore.data.Comment
import com.example.nikestore.services.http.ApiService
import io.reactivex.Single

class CommentRemoteDataSource(val apiService: ApiService) :CommentDataSource {
    override fun getComments(productId:Int): Single<List<Comment>> {
        return apiService.getComments(productId)
    }

    override fun addComment(): Single<Comment> {
        TODO("Not yet implemented")
    }
}