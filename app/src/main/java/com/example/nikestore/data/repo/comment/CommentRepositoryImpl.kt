package com.example.nikestore.data.repo.comment

import com.example.nikestore.data.Comment
import com.example.nikestore.data.repo.sourse.CommentDataSource
import io.reactivex.Single

class CommentRepositoryImpl(val commentDataSource: CommentDataSource): CommentRepository {
    override fun getComments(productId:Int): Single<List<Comment>> {
        return commentDataSource.getComments(productId)
    }

    override fun addComment(): Single<Comment> {
        TODO("Not yet implemented")
    }

}