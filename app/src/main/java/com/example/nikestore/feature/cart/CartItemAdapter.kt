package com.example.nikestore.feature.cart

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.menu.MenuView
import androidx.recyclerview.widget.RecyclerView
import com.example.nikestore.R
import com.example.nikestore.common.formatPrice
import com.example.nikestore.data.CartItem
import com.example.nikestore.data.PurchaseDetail
import com.example.nikestore.services.ImageLoadingService
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_cart.view.*
import kotlinx.android.synthetic.main.item_purchase_details.view.*

const val VIEW_TYPE_CART_ITEM = 0
const val VIEW_TYPE_PURCHASE_DETAILS = 1

class CartItemAdapter(
    val cartItems: MutableList<CartItem>,
    val imageLoadingService: ImageLoadingService,
    val cartItemViewCallbacks: CartItemViewCallbacks
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var purchaseDetail: PurchaseDetail? = null

    inner class CartItemViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bindCartItem(cartItem: CartItem) {
            containerView.productTitleTv_itemCart.text = cartItem.product.title
            containerView.cartItemCountTv_cartItem.text = cartItem.count.toString()
            containerView.previousPriceTv_cartItem.text =
                formatPrice(cartItem.product.price + cartItem.product.discount)
            containerView.currentPriceTv_cartItem.text = formatPrice(cartItem.product.price)
            imageLoadingService.load(containerView.productIv_itemCart, cartItem.product.image)

            containerView.removeFromCartBtn_cartItem.setOnClickListener {
                cartItemViewCallbacks.onRemoveCartItemButtonClick(cartItem)

            }

            containerView.changeCountProgressBar.visibility =
                if (cartItem.changeCountprogressBarIsVisible) View.VISIBLE else View.GONE

            containerView.cartItemCountTv_cartItem.visibility =
                if (cartItem.changeCountprogressBarIsVisible) View.INVISIBLE else View.VISIBLE

            containerView.increaseBtn_cartItem.setOnClickListener {
                if (cartItem.count < 5) {
                    cartItem.changeCountprogressBarIsVisible = true
                    containerView.changeCountProgressBar.visibility = View.VISIBLE
                    containerView.cartItemCountTv_cartItem.visibility = View.INVISIBLE
                    cartItemViewCallbacks.onIncreaseCartItemButtonClick(cartItem)
                }
            }


            containerView.decreaseBtn_cartItem.setOnClickListener {
                if (cartItem.count > 1) {
                    cartItem.changeCountprogressBarIsVisible = true
                    containerView.changeCountProgressBar.visibility = View.VISIBLE
                    containerView.cartItemCountTv_cartItem.visibility = View.INVISIBLE
                    cartItemViewCallbacks.onDecreaseCartItemButtonClick(cartItem)
                }
            }

            containerView.productIv_itemCart.setOnClickListener {
                cartItemViewCallbacks.onProductImageClick(cartItem)
            }

            if (cartItem.changeCountprogressBarIsVisible) {
                containerView.productIv_itemCart.visibility = View.VISIBLE
                containerView.increaseBtn_cartItem.visibility = View.GONE
            }

        }
    }

    class PurchaseDetailViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(totalPrice: Int, shippingCost: Int, payblePrice: Int) {
            containerView.totalPriceTv_itemPurchaseDetail.text = formatPrice(totalPrice)
            containerView.shippingCostTv_itemPurchaseDetail.text = formatPrice(shippingCost)
            containerView.payblePriceTv_itemPurchaseDetail.text = formatPrice(payblePrice)
        }
    }

    interface CartItemViewCallbacks {
        fun onRemoveCartItemButtonClick(cartItem: CartItem)
        fun onIncreaseCartItemButtonClick(cartItem: CartItem)
        fun onDecreaseCartItemButtonClick(cartItem: CartItem)
        fun onProductImageClick(cartItem: CartItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_CART_ITEM) {
            return CartItemViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_cart, parent, false)
            )
        } else {
            return PurchaseDetailViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_purchase_details, parent, false)
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CartItemViewHolder) {
            holder.bindCartItem(cartItems[position])
        } else if (holder is PurchaseDetailViewHolder) {
            purchaseDetail?.let {
                holder.bind(it.totalPrice, it.shipping_cost, it.payable_price)
            }
        }
    }

    override fun getItemCount(): Int {
        return cartItems.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        if (position == cartItems.size) {
            return VIEW_TYPE_PURCHASE_DETAILS
        } else {
            return VIEW_TYPE_CART_ITEM
        }
    }

    fun removeCartItem(cartItem: CartItem) {
        val index = cartItems.indexOf(cartItem)
        if (index > -1) {
            cartItems.removeAt(index)
            notifyItemRemoved(index)
        }
    }

    fun increaseCount(cartItem: CartItem) {
        val index = cartItems.indexOf(cartItem)
        if (index > -1) {
            cartItems[index].changeCountprogressBarIsVisible = false
            notifyItemChanged(index)
        }
    }

    fun decreaseCount(cartItem: CartItem) {
        val index = cartItems.indexOf(cartItem)
        if (index > -1) {
            cartItems[index].changeCountprogressBarIsVisible = false
            notifyItemChanged(index)
        }
    }
}