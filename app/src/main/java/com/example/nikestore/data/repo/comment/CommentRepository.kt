package com.example.nikestore.data.repo.comment

import com.example.nikestore.data.Comment
import io.reactivex.Single

interface CommentRepository {

    fun getComments(productId:Int):Single<List<Comment>>

    fun addComment():Single<Comment>
}