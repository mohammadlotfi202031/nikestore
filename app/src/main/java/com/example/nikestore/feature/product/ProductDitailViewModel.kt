package com.example.nikestore.feature.product

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import com.example.nikestore.common.EXTRA_KEY_DATA
import com.example.nikestore.common.NikeSingleObserver
import com.example.nikestore.common.NikeViewModel
import com.example.nikestore.common.asyncNetworkRequest
import com.example.nikestore.data.Comment
import com.example.nikestore.data.Product
import com.example.nikestore.data.repo.cart.CartRepository
import com.example.nikestore.data.repo.comment.CommentRepository
import io.reactivex.Completable

class ProductDitailViewModel    (bundle: Bundle, commentRepository: CommentRepository, val cartRepository: CartRepository) : NikeViewModel() {

    val productLivedata = MutableLiveData<Product>()
    val commentsLiveData=MutableLiveData<List<Comment>>()

    init {

        productLivedata.value = bundle.getParcelable(EXTRA_KEY_DATA)
        progressBarLiveData.value=true
        commentRepository.getComments(productLivedata.value!!.id)
            .asyncNetworkRequest()
            .doFinally{progressBarLiveData.value=false}
            .subscribe(object :NikeSingleObserver<List<Comment>>(compositeDisposable){
                override fun onSuccess(t: List<Comment>) {
                    commentsLiveData.value=t
                }
            })
    }

    fun onAddToCartBtn():Completable{
        return cartRepository.addToCart(productLivedata.value!!.id).ignoreElement()
    }
}