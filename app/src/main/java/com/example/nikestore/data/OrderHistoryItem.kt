package com.example.nikestore.data

data class OrderHistoryItem(
    val order_items: List<OrderItem>,
    val id: Int,
    val payable: Int
)