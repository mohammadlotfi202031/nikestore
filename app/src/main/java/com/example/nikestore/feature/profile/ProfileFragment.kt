package com.example.nikestore.feature.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.nikestore.R
import com.example.nikestore.common.NikeFragment
import com.example.nikestore.feature.auth.AuthActivity
import com.example.nikestore.feature.favorite.FavoriteProductsActivity
import com.example.nikestore.feature.order.OrderHistoryActivity
import kotlinx.android.synthetic.main.activity_check_out.*
import kotlinx.android.synthetic.main.fragment_profile.*
import org.koin.android.ext.android.inject

class ProfileFragment: NikeFragment() {

    private val viewmodel:ProfileViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_profile,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tv_favoriteProduct.setOnClickListener {
            startActivity(Intent(requireContext(),FavoriteProductsActivity::class.java))
        }

        orderHistoryTV_profile.setOnClickListener {
            startActivity(Intent(context,OrderHistoryActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()
        checkAuthState()
    }

    private fun checkAuthState() {
        if (viewmodel.isSignedIn){
            authBtn_profile.text=getString(R.string.signOutScreenTitle)
            authBtn_profile.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_signout,0)
            userNameTv_profile.text=viewmodel.username
            authBtn_profile.setOnClickListener{
                viewmodel.signOut()
                checkAuthState()
            }
        }else{
            authBtn_profile.text=getString(R.string.loginScreenTitle)
            authBtn_profile.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_login,0)
            userNameTv_profile.text=getString(R.string.guest_user)
            authBtn_profile.setOnClickListener{
                startActivity(Intent(requireContext(),AuthActivity::class.java))
            }
        }
    }

}